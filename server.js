'use strict'

var express = require('express')
var morgan = require('morgan')
var path = require('path')
var cors = require('cors')
var app = express()

var mongoose = require('mongoose')
var bodyParser = require('body-parser')
var config = require('./env/config')

// DB Connection
mongoose.connect(config.URL, { useNewUrlParser: true }, () => {
  console.log('mLab DB connection established')
})

app.use(morgan('dev'))
app.use(cors({origin: 'http://localhost:8080'}))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: true}))

var port = config.PORT || 5000

app.listen(port) 

console.log('Listening on port ' + port)

var todoRoutes = require('./controllers/routes')

//  Use routes defined in Route.js and prefix with todo
app.use('/api', todoRoutes)

app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:8080');
    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', false);
    // Pass to next layer of middleware
    next();
})

app.get('/', function (req, res, next) {
  res.json('Hello')
})